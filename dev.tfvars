region = "us-east-1"

app = "app"
env = "dev"

compute_vpc_cidr = "10.0.0.0/21"
compute_private_subnets_cidr = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
compute_public_subnets_cidr = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]


data_vpc_cidr = "10.0.8.0/21"
data_private_subnets_cidr = ["10.0.8.0/24", "10.0.9.0/24", "10.0.10.0/24"]
data_public_subnets_cidr = ["10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24"]
