variable "region" {
	type = string
  default = "us-east-1"
}

variable "azs" {
	type = list(string)
  default = ["us-east-1a","us-east-1b","us-east-1c"]
}

variable "app" {
  type = string
  default = "app"
}

variable "env" {
  type = string
  default = "dev"
}

// VPC variables
variable "compute_vpc_cidr" {
  type = string
  description = "CIDR for Compute VPC"
}

variable "compute_public_subnets_cidr" {
  type = list(string)
  description = "CIDR for Compute VPC public subnets"
}

variable "compute_private_subnets_cidr" {
  type = list(string)
  description = "CIDR for Compute VPC private subnets"
}

variable "data_vpc_cidr" {
  type = string
  description = "CIDR for Data VPC"
}

variable "data_public_subnets_cidr" {
  type = list(string)
  description = "CIDR for Data VPC public subnets"
}

variable "data_private_subnets_cidr" {
  type = list(string)
  description = "CIDR for Data VPC private subnets"
}

# EC2 node group variables
variable "k8s_node_group_ami" {
  type = string
  description = "AMI for K8S node group"
  default = "ami-0b0fc74d726245a8b"
}

variable "k8s_node_group_node_type" {
  type = string
  description = "Node type for K8S node group"
  default = "t3.medium"
}
