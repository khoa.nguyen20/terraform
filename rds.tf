resource "aws_db_subnet_group" "this" {
  provider = aws.data
  name       = "${var.app}-${var.env}-database-subnet-group"
  subnet_ids = module.data_vpc.private_subnets

  tags = {
    Terraform   = "true"
    Application = var.app
    Environment = var.env
  }
}

resource "aws_rds_cluster" "rds" {
  provider = aws.data
  cluster_identifier        = "${var.app}-${var.env}-database"
  db_subnet_group_name      = aws_db_subnet_group.this.id
  engine                    = "mysql"
  engine_version            = "5.7"
  db_cluster_instance_class = "db.t3.small"
  allocated_storage         = 100
  master_username           = "test"
  master_password           = "mustbeeightcharaters"

  tags = {
    Terraform   = "true"
    Application = var.app
    Environment = var.env
  }
}