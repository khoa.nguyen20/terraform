resource "aws_launch_template" "k8s_node_group" {
  name   = "${var.app}-${var.env}-k8s-node-group-launch-tpl"
  image_id      = var.k8s_node_group_ami
  instance_type = var.k8s_node_group_node_type

  tags = {
    Terraform   = "true"
    Application = var.app
    Environment = var.env
  }
}

resource "aws_autoscaling_group" "k8s_node_group" {
  name = "${var.app}-${var.env}-k8s-node-group"
  desired_capacity   = 0
  max_size           = 10
  min_size           = 0
  vpc_zone_identifier = module.compute_vpc.private_subnets

  launch_template {
    id      = aws_launch_template.k8s_node_group.id
    version = "$Latest"
  }
}
