module "compute_vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.12.0"

  name = "${var.app}-${var.env}-compute-vpc"
  cidr = var.compute_vpc_cidr

  azs             = var.azs
  private_subnets = var.compute_private_subnets_cidr
  public_subnets  = var.compute_public_subnets_cidr

  enable_nat_gateway  = true
  single_nat_gateway  = true
  enable_dns_hostnames = true

  tags = {
    Terraform   = "true"
    Application = var.app
    Environment = var.env
  }
}

module "data_vpc" {
  providers = {
    aws = aws.data
  }
  source = "terraform-aws-modules/vpc/aws"
  version = "3.12.0"

  name = "${var.app}-${var.env}-data-vpc"
  cidr = var.data_vpc_cidr

  azs             = var.azs
  private_subnets = var.data_private_subnets_cidr
  public_subnets  = var.data_public_subnets_cidr

  enable_nat_gateway  = true
  single_nat_gateway  = true
  enable_dns_hostnames = true

  tags = {
    Terraform   = "true"
    Application = var.app
    Environment = var.env
  }
}

data "aws_caller_identity" "data" {
  provider = aws.data
}

# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = module.compute_vpc.vpc_id
  peer_vpc_id   = module.data_vpc.vpc_id
  peer_owner_id = data.aws_caller_identity.data.account_id
  peer_region   = "us-east-1"
  auto_accept   = false

  tags = {
    Side = "Requester"
  }
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.data
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
  }
}

resource "aws_route" "private_compute_to_data" {
  for_each = {for i, val in module.compute_vpc.private_route_table_ids: i => val}

  route_table_id = each.value

  destination_cidr_block = module.data_vpc.vpc_cidr_block

  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id

  depends_on = [
    module.compute_vpc
  ]
}

resource "aws_route" "private_data_to_compute" {
  for_each = {for i, val in module.data_vpc.private_route_table_ids: i => val}

  route_table_id = each.value

  destination_cidr_block = module.compute_vpc.vpc_cidr_block

  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id

  depends_on = [
    module.compute_vpc
  ]
}