# Provider for Compute VPC
provider "aws" {
  region  = var.region
  profile = "default"
}

# Provider for database VPC
provider "aws" {
  alias  = "data"
  region  = var.region
  profile = "default"
}